module.exports = {
  apps: [
    {
      name: 'nodejs template express app',
      script: './dist/index.js',
      instances: 'MAX',
      args: 'Express app process - 1',
      watch: false,
      autorestart: true,
      exec_mode: 'cluster',
      max_memory_restart: '900M',
      env: {
        NODE_ENV: 'local',
        PORT: 4006,
      },
      env_production: {
        NODE_ENV: 'production',
        PORT: 80,
      },
    },
  ],
};
