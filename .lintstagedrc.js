module.exports = {
  '*.ts': [
    'eslint --max-warnings=20',
    'yarn run test:unit --bail --findRelatedTests --passWithNoTests',
    () => 'tsc-files --noEmit',
  ],
  '*.{js,jsx,ts,tsx,json,css,js}': ['prettier --write'],
};
