module.exports = [
  // company 1
  {
    id: "1",
    name: "SecondBite",
  },
  // company 2
  {
    id: "2",
    name: "Axil Coffee Roasters",
  },
  // company 3
  {
    id: "3",
    name: "MYER",
  },
  // company 4
  {
    id: "4",
    name: "David Jones",
  },
];
