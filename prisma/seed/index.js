const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const users = require("./user");
const companies = require("./company");
const products = require("./product");
const discounts = require("./discount");

async function runSeeders() {
  // companies
  await Promise.all(
    companies.map(async (c) =>
      prisma.company.upsert({
        where: { id: c.id },
        update: {},
        create: c,
      }),
    ),
  );

  // products
  await Promise.all(
    products.map(async (p) =>
      prisma.product.upsert({
        where: { id: p.id },
        update: {},
        create: p,
      }),
    ),
  );

  // Users
  await Promise.all(
    users.map(async (user) =>
      prisma.user.create({
        data: {
          id: user.id,
          password: user.password,
          username: user.username,
          belongsToId: user.belongsToId,
        },
      }),
    ),
  );

  // discounts
  await Promise.all(
    discounts.map(async (d) =>
      prisma.discount.upsert({
        where: { id: d.id },
        update: {},
        create: d,
      }),
    ),
  );
}

runSeeders()
  .catch((e) => {
    console.error(`There was an error while seeding: ${e}`);
    process.exit(1);
  })
  .finally(async () => {
    console.log("Successfully seeded database. Closing connection.");
    await prisma.$disconnect();
  });
