module.exports = [
  // Gets a 3 for 2 deal on Classic Ads
  {
    id: "1",
    count: 3,
    amount: 269.99,
    belongsToCompanyId: "1", // SecondBite
    belongsToProductId: "1", // Classic Ad
  },
  // Gets a discount on Stand out Ads where the price drops to $299.99 per ad
  {
    id: "2",
    count: 1,
    amount: 23.0,
    belongsToCompanyId: "2", // Axil Coffee Roasters
    belongsToProductId: "2", // Stand out Ad
  },
  // Gets a 5 for 4 deal on Stand out Ads
  {
    id: "3",
    count: 5,
    amount: 322.99,
    belongsToCompanyId: "3", // MYER
    belongsToProductId: "2", // Stand out Ad
  },
  // Gets a discount on Premium Ads where the price drops to $389.99 per ad
  {
    id: "4",
    count: 1,
    amount: 5.0,
    belongsToCompanyId: "3", // MYER
    belongsToProductId: "3", // Premium Ad
  },
];
