module.exports = [
  // user 1
  {
    id: "1",
    password: "$2b$12$J.2vYWoNpI2cp2UzeNoUwOytJKkeyWi0igqWnhQQP5UEGMzs9pc..", // password is Tom
    username: "Tom",
    belongsToId: "1", // SecondBite
  },
  // user 2
  {
    id: "2",
    password: "$2b$12$62wvaB8X7BiyVlVyHorFkuzWDUAUtECWF8NjeRyeCI2fmWmzVTA9e", // password is Jerry
    username: "Jerry",
    belongsToId: "2", // Axil Coffee Roasters
  },
  // user 3
  {
    id: "3",
    password: "$2b$12$FIGT766D51JNZCaZgZYvWeup0EcoLHWUN5gKTRCzXYV/eq5YI0uGC", // password is David
    username: "David",
    belongsToId: "3", // MYER
  },
  // regular Customer has no discount
  {
    id: "4",
    password: "$2b$12$Zpm5psyM9riVCnwnc3J/EO6sJFMtHfGbjl4Y2LSANb2VFsOVv9xei", // password is Peter
    username: "Peter",
    belongsToId: "4", // David Jones
  },
];
