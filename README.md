## local run orchestration with dockerized express app and postgres
### run
docker-compose up --build -d <br />
yarn run migrate:postgres:local <br />
yarn run seed:local <br />
see database interface by: yarn run show:db:local

### play around
for discount test, play login following logic in ./prisma/seed/user.js <br />
send POST request to http://localhost:4000/signin with body {"username": "Tom", "password": "Tom"} <br />
SecondBite user login<br />
username: Tom <br />
password: Tom <br />

Axil Coffee Roasters user login<br />
username: Jerry <br />
password: Jerry <br />

MYER user login<br />
username: David <br />
password: David <br />

default customer (without company discount) login<br />
username: Peter <br />
password: Peter <br />

then use the received JWT, in request header as bearer token, to call endpoint: http://localhost:4000/api/checkoutPrice<br />
{
  "items": [
    {
      "id": "1",
      "amount": 3
    },
    {
      "id": "2",
      "amount": 1
    },
    {
      "id": "3",
      "amount": 1
    }
  ]
}
<br />
"id" in the request body: is the product ID which frontend will cache and sendback, please compare to ./prisma/seed/product.js, and change different product's buying amount to play around. For example: <br />
Items: `classic`, `standout`, `premium`
{
  "items": [
    {
      "id": "1",
      "amount": 1
    },
    {
      "id": "2",
      "amount": 1
    },
    {
      "id": "3",
      "amount": 1
    }
  ]
} 
<br />
result will be $987.97 for default user login <br />

Items: `classic`, `classic`, `classic`, `premium`
{
  "items": [
    {
      "id": "1",
      "amount": 3
    },
    {
      "id": "3",
      "amount": 1
    }
  ]
}
<br />
result will be $934.97 for SecondBite user login <br />

Items: `standout`, `standout`, `standout`, `premium`
{
  "items": [
    {
      "id": "2",
      "amount": 3
    },
    {
      "id": "3",
      "amount": 1
    }
  ]
}
<br />
result will be $1294.96 for Axil Coffee Roasters user login <br />

### stop
docker-compose down -v

## Features
### discount calculating design
please check db schema logic under ./prisma/schema.prisma and ./prisma/seed/*<br />
1. one user is belonged to a company, can login with username/password
2. token will generated after login, please use it to send request to /api/checkoutPrice
3. one discount logic will be applied to one company and one product only, so added a unique compound index for it 
4. once longin user who has a company id in token, and product id in request body, the discount logic will be clear to each user (for some default user, will have no discount)
5. after apply discount, endpoint will send back to total cost for current shopping

### Prisma ORM

### simple server side logging

### JWT protected endpoints

### maintain code quality with typescript check, linting check, prettier, husky, commit lint
yarn run test:lint<br />
yarn run test:typescript

### unit test
yarn run test:coverage

### local orchestration test with docker compose

### pipeline 
since no time to setup a jenkins or codefresh server, so just write some pseudocode pipeline codes for codeBuild and codeDeploy<br />
./buildspec.yml and ./appspec.yml<br />

## use pm2 in prod server instead of dockerized
### pre-required commands
npm install -g pm2 <br />
yarn install <br />
yarn build <br />
npx prisma generate

### prepare env file
.env.production

### if have postgres connection, and first time run it, be careful since its production
yarn run dangerous:migrate:postgres:prod

### start app with pm2 in prod
pm2 start ecosystem.config.js --env production

### stop the app
pm2 stop all

### delete the app process
pm2 delete all

## To Do
### enable integration test with super test
currently, need to setup a test env db so can do integration test against it

### enable frontend
as requested, only focus on backend for this project, but if needed, can quickly deliver frontend part as well..

### setup pipeline server

### enable price check for not login user as well..

### K8s configure + helm deployment

### although single developer, still need to protect main branch with git flow