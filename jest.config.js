/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testResultsProcessor: "jest-sonar-reporter",
  coveragePathIgnorePatterns: ["<rootDir>/dist/"],
  coverageDirectory: "./testReport",
  coverageReporters: ["text", "html"],
  reporters: [
    "default",
    [
      "jest-junit",
      {
        outputDirectory: ".",
        outputName: "unit-results.xml",
      },
    ],
    [
      "jest-html-reporters",
      {
        publicPath: "./testReport",
        filename: "unit-results.html",
      },
    ],
  ],
};
