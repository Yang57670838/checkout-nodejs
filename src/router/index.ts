import { Router } from "express";
import { requestErrorHandler } from "../modules/validate";
import { priceCal } from "../handlers/checkout";
import { reqValidationCheckout } from "../handlers/validate";

const router = Router();

router.post("/checkoutPrice", reqValidationCheckout, requestErrorHandler, priceCal);
export default router;
