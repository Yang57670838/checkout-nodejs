import app from "./server";
import * as dotenv from "dotenv";
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

app.listen(process.env.PORT || 4000, () => {
  console.log(`server on http://localhost:${process.env.PORT || 4000}`);
});
