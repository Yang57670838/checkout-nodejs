export const validTokenNoExpire =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1MTIzMjQ1IiwiZW1haWwiOiJhd2FAZ21haWwuY29tIiwiYmVsb25nc1RvSWQiOiIxMjM0NSIsInVzZXJuYW1lIjoidG9tIiwicm9sZSI6IkFETUlOIiwibmFtZSI6IkRhdmlkIEpvbmVzIiwiaWF0IjoxNjcyNzMyNzcyfQ.4eze6JGBLBu_iXAqMTZ9uZO4hEd8m9fvT6qDEJIT5OU";

export const mockDecodedUser = {
  username: "tom",
  email: "awa@gmail.com",
  belongsToId: "12345",
  role: "ADMIN",
  name: "David Jones",
  id: "12345123245",
};
