import { hashPassword, protect, comparePasswords } from "../auth";
import { validTokenNoExpire, mockDecodedUser } from "../../mocks/auth";

describe("hashPassword modules", () => {
  it("should generate salt, salt level with hashed password together", async () => {
    const data = await hashPassword("password");
    expect(data.split("$").length).toEqual(4);
    expect(data.split("$")[2]).toEqual("12");
  });
});

describe("comparePasswords modules", () => {
  it("should compare password with hashed value correctly", async () => {
    const data = await comparePasswords("Tom", "$2b$12$J.2vYWoNpI2cp2UzeNoUwOytJKkeyWi0igqWnhQQP5UEGMzs9pc..");
    expect(data).toEqual(true);
    const data2 = await comparePasswords("Jerry", "$2b$12$62wvaB8X7BiyVlVyHorFkuzWDUAUtECWF8NjeRyeCI2fmWmzVTA9e");
    expect(data2).toEqual(true);
    const data3 = await comparePasswords("David", "$2b$12$FIGT766D51JNZCaZgZYvWeup0EcoLHWUN5gKTRCzXYV/eq5YI0uGC");
    expect(data3).toEqual(true);
    const data4 = await comparePasswords("Peter", "$2b$12$Zpm5psyM9riVCnwnc3J/EO6sJFMtHfGbjl4Y2LSANb2VFsOVv9xei");
    expect(data4).toEqual(true);
  });
});

describe("protect middleware", () => {
  const noTokenReq: any = {
    headers: {},
  };
  const notValidTokenReq: any = {
    headers: {
      authorization: "11111",
    },
  };
  // mock token
  const validTokenReq: any = {
    headers: {
      authorization: `Bearer ${validTokenNoExpire}`,
    },
  };
  const mockResponse: any = {
    status: jest.fn(),
    json: jest.fn(),
    send: jest.fn(),
  };
  const nextFunction = jest.fn();

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should return 401 if not auth, and prevent passing to next route", async () => {
    protect(noTokenReq, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(401);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "not authorized",
    });
    expect(nextFunction).not.toHaveBeenCalled();
  });

  it("should return 401 if pass with a not valid token, and prevent passing to next route", async () => {
    protect(notValidTokenReq, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(401);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "not valid token",
    });
    expect(nextFunction).not.toHaveBeenCalled();
  });

  it("should pass to next route if valid token", async () => {
    protect(validTokenReq, mockResponse, nextFunction);
    expect(validTokenReq.user.role).toEqual(mockDecodedUser.role);
    expect(validTokenReq.user.username).toEqual(mockDecodedUser.username);
    expect(validTokenReq.user.email).toEqual(mockDecodedUser.email);
    expect(validTokenReq.user.id).toEqual(mockDecodedUser.id);
    expect(nextFunction).toHaveBeenCalled();
  });
});
