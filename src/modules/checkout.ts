import { Discount } from "@prisma/client";

export const discountPriceCal = (itemCount: number, originalCost: number, discount: Discount): number => {
  const result = Math.floor(itemCount / discount.count) * discount.amount.toNumber();
  return originalCost - result;
};
