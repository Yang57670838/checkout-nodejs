import { Request, Response, NextFunction } from "express";
import prisma from "../db";
import { createJWT, comparePasswords } from "../modules/auth";

export const signin = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await prisma.user.findUnique({
      where: {
        username: req.body.username,
      },
    });
    if (user) {
      const isValid = await comparePasswords(req.body.password, user.password);
      if (!isValid) {
        res.status(401);
        res.json({ message: "not valid token" });
        return;
      }
      const token = createJWT(user);
      res.json({ token });
    }
  } catch (e) {
    next(e);
  }
};
