import { body } from "express-validator";

export const reqValidationLogin = [body("username").isString(), body("password").trim().isString()];

// need a list of checkout items,
export const reqValidationCheckout = [
  body("items").isArray(), // TODO: refactor this check towards strict interface
];
