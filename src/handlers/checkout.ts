import { Request, Response, NextFunction } from "express";
import { discountPriceCal } from "../modules/checkout";
import prisma from "../db";

export const priceCal = async (req: Request, res: Response) => {
  let totalCost = 0;
  const listOfItems = req.body.items;
  for (const item of listOfItems) {
    //find single product original price first
    const singleProduct = await prisma.product.findUnique({
      where: {
        id: item.id,
      },
    });
    // n * same item cal
    const oneDiscount = await prisma.discount.findUnique({
      where: {
        belongsToCompanyId_belongsToProductId: {
          belongsToCompanyId: req.user.belongsToId, // check login user's company have which discount
          belongsToProductId: item.id,
        },
      },
    });
    if (singleProduct) {
      const originalCost = singleProduct.price.toNumber() * item.amount;
      if (oneDiscount) {
        const result = discountPriceCal(item.amount, originalCost, oneDiscount);
        totalCost = totalCost + result;
      } else {
        totalCost = totalCost + originalCost;
      }
    }
  }
  res.json({ totalCost });
};
