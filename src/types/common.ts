export interface ExpressError extends Error {
  type: string;
}

declare module "express-serve-static-core" {
  export interface Request {
    user: any;
  }
}
