import express, { Application } from "express";
import router from "./router";
import morgan from "morgan";
import cors from "cors";
import { protect } from "./modules/auth";
import { signin } from "./handlers/user";
import { rootHandler, ErrorHandler } from "./handlers/root";
import { requestErrorHandler } from "./modules/validate";
import { reqValidationLogin } from "./handlers/validate";

const app: Application = express();
const isProd = process.env.NODE_ENV === "production";

app.use(morgan(isProd ? "common" : "dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get("/", rootHandler);

app.use("/api", protect, router);
app.post("/signin", reqValidationLogin, requestErrorHandler, signin);

// global error handler
app.use(ErrorHandler);

export default app;
